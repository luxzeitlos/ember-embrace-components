import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('my-route');
  this.route('parent', function() {
    this.route('child');
  });
  this.route('with-query-params');
});
