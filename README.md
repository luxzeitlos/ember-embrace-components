ember-embrace-components
==============================================================================

Warning: this is so far an *experiment*!
I wanted to explore how a new ember API could look like. Feedback is welcome.
But for now I would recommend that you *dont use this in production*!

This addon tries to provides and experimental way to use ember without
controllers and route templates. Instead there is a component for every
route under `app/components/routes`.


Compatibility
------------------------------------------------------------------------------

* Ember.js v3.12 or above
* Ember CLI v2.13 or above
* Node.js v10 or above


Installation
------------------------------------------------------------------------------

```
ember install ember-embrace-components
```


Usage
------------------------------------------------------------------------------

For now for this to work the `templates` folder should be removed and all
components should use co-location. Also every route except the `application`,
`index` and all nested `index` routes *must* have a `.js` file. Just declaring
the route in the `router.js` will not be enough.

next you can use route components. So for the `foo` route you would have a file
`app/components/routes/foo.hbs`. In the file you have access to `{{@model}}`.
You can also have a component file at `app/components/routes/foo.js` where you
have access to `this.args.model`.

If your route has sub routes you can use `{{yield}}` instead of `{{outlet}}`
to declare a placeholder for them. If you dont declare a route component
for your route a default component with just `{{yield}}` will be generated
for you. This matches the current behaviour of route templates.

## queryParams

It is possible to use queryParams with route components.
For this there must be a named export called `queryParams`on the `route`
declaring the default values:

```
import Route from '@ember/routing/route';

export const queryParams = {
  myQueryParam: 'default value',
};

export default class WithQueryParamsRoute extends Route {
}
```

There query param can be accessed from the route component:

```
{{@queryParams.myQueryParam}}
```

The `queryParams` object has settable properties. So this will work:

export default class WithQueryParamsComponent extends Component {
  @action setQueryParam() {
    this.args.queryParams.myQueryParam = "something new";
  }
}

Or the query param can be bound to an input:

```
<Input @value={{@queryParams.myqueryParam}}>
```

## about blueprints

currently if you do `ember generate route my-route` you'll need to delete the
`app/templates/my-route.hbs` file after.

Contributing
------------------------------------------------------------------------------

See the [Contributing](CONTRIBUTING.md) guide for details.


License
------------------------------------------------------------------------------

This project is licensed under the [MIT License](LICENSE.md).
