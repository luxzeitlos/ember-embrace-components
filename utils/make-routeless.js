const Plugin = require('broccoli-plugin');
const fs = require('fs');
const path = require('path');
const walkSync = require('walk-sync');
const mkdirp = require('mkdirp')

module.exports = class MakeRouteless extends Plugin {
  constructor(inputNodes, options = {}) {
    super(inputNodes, {
      annotation: options.annotation,
      // see `options` in the below README to see a full list of constructor options
    });
  }

  get appName() {
    const [appName] = fs.readdirSync(this.inputPaths[0]);
    return appName;
  }

  get inputAppPath() {
    return path.join(this.inputPaths[0], this.appName)
  }

  get outputAppPath() {
    return path.join(this.outputPath, this.appName)
  }

  setupRoute(routeSegments) {
    const routeInvocationString = routeSegments
      .map(part => part.split('-').map(seg => `${seg[0].toUpperCase()}${seg.substr(1)}`).join(''))
      .join('::')
    const outputRouteTemplatePath = path.join(this.outputAppPath, 'templates', ...routeSegments) + '.js';
    const inputRouteComponentPath = path.join(this.inputAppPath, 'components', 'routes', ...routeSegments) + '.js';
    const outputRouteComponentPath = path.join(this.outputAppPath, 'components', 'routes', ...routeSegments) + '.js';
    const outputControllerPath = path.join(this.outputAppPath, 'controllers', ...routeSegments) + '.js';
    const inputRoutePath = path.join(this.inputAppPath, 'routes', ...routeSegments) + '.js';

    console.log('write', outputRouteTemplatePath);
    mkdirp.sync(path.dirname(outputRouteTemplatePath));
    fs.writeFileSync(outputRouteTemplatePath, `
      import { hbs } from 'ember-cli-htmlbars';
      export default hbs\`
        <Routes::${routeInvocationString} @model={{@model}} @queryParams={{this.queryParamsAccessObject}}>
          {{outlet}}
        </Routes::${routeInvocationString}>
      \`;
    `);

    if(!fs.existsSync(inputRouteComponentPath)) {
      mkdirp.sync(path.dirname(outputRouteComponentPath));
      fs.writeFileSync(outputRouteComponentPath, `
        import { hbs } from 'ember-cli-htmlbars';
        const __COLOCATED_TEMPLATE__ = hbs\`{{yield}}\`;

        import templateOnly from '@ember/component/template-only';
        export default templateOnly();
      `)
    }

    if(fs.existsSync(inputRoutePath)) {
      mkdirp.sync(path.dirname(outputControllerPath));
      fs.writeFileSync(outputControllerPath, `
        import { queryParams } from '${this.appName}/routes/${routeSegments.join('/')}';
        import makeFakeController from 'ember-embrace-components/utils/make-fake-controller';

        export default makeFakeController({ queryParams });
      `);
    }
  }

  build() {
    const routeDirTree = walkSync(path.join(this.inputAppPath, 'routes'));
    const routes = [
      'application',
      'index',
      ...routeDirTree
        .filter(x => x.endsWith('/'))
        .map(x => `${x}index`),
      ...routeDirTree
        .filter(x => x.endsWith('.ts') || x.endsWith('.js'))
        .map(x => x.slice(0, -3))
    ].sort().map(r => r.split('/'));

    for(const route of routes) {
      this.setupRoute(route);
    }
    console.log('written', this.outputPath);
    // const input = this.input.readFileSync(`foo.txt`);
    // const output = someCompiler(input);

    // Write to 'bar.txt' in this node's output
    // this.output.writeFileSync(`bar.txt`, "test");
  }
}
