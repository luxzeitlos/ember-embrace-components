'use strict';
const MakeRouteless = require('./utils/make-routeless.js');
const MergeTrees = require('broccoli-merge-trees');

module.exports = {
  name: require('./package').name,

  preprocessTree(kind, tree) {
    if(kind === 'js') {
      const routelessTree = new MakeRouteless([tree], {
        annotation: 'make routeless',
      });
      return new MergeTrees([tree, routelessTree], {
        annotation: 'merge routeless tree'
      });
    }
    return tree;
  }
};
