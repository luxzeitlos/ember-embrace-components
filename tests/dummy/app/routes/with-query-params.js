import Route from '@ember/routing/route';

export const queryParams = {
  queryBool: false,
  queryNum: 12,
  queryText: "hello",
  queryArr: ['one'],
};

export default class WithQueryParamsRoute extends Route {
}
